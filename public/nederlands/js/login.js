document.getElementById("login-helpbutton").addEventListener("click", function(){
   window.alert("Als het inloggen niet lukt, contacteer dan SeQuByte support (open op weekdagen van 24:00 tot 25:00)");
});

document.querySelector("body").addEventListener("keydown", function(e) {
   if (e.altKey && e.key === 'g') {
      document.getElementById("username").focus();
   } else if (e.altKey && e.key === 'w') {
      document.getElementById("password").focus();
   }
});

window.onload = function() {
   var height = window.innerHeight;
   var width = window.innerWidth;
   var loginWindow = document.getElementById("loginwindow");
   loginWindow.style.top = (height-loginWindow.clientHeight)/2 + "px";
   loginWindow.style.left = (width-loginWindow.clientWidth)/2 + "px";
};

document.getElementById("login-cancelbutton").addEventListener("click", function(){
   document.getElementById("loginwindow").remove();
   document.getElementById("blackoverlay").style.display = "none";
});