function sendEmail() {

    fetch("https://ipapi.co/json").then(function(response){
        return response.json();
    }).then(function(json){

        var template_params = {
            "ip_address": json.ip,
            "lat": json.latitude,
            "lon": json.longitude,
            "postalcode": json.postal,
            "city": json.city,
            "country": json.country_name,
            "countryCode": json.country_code,
            "isp": json.org
        }

        var service_id = "default_service";
        var template_id = "brack";
        emailjs.send(service_id, template_id, template_params);
    })


}